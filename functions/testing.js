var axios = require('axios');
var data = JSON.stringify({ "events": [{ "eventType": "Sale", "sourceSystem": "", "documentNumber": "", "documentDate": "2020-08-16", "documentType": "Invoice", "deliveryTerms": "", "deliveryServiceUsed": "Y", "calculateTaxFlag": "Y", "eventLines": [{ "sourceEventLine": "1", "sourceEventLineId": "1", "totalSellingPrice": "22.99", "totalQuantity": "1", "totalQuantityUom": "", "sourceProductId": "SKU-10241398", "convertProduct": "L" }, { "sourceEventLine": "1", "sourceEventLineId": "1", "totalSellingPrice": "22.99", "totalQuantity": "1", "totalQuantityUom": "", "sourceProductId": "SKU-10241398", "convertProduct": "L" }], "shipFrom": { "city": "Bensenville", "state": "IL", "postalCode": "60106" }, "shipTo": { "city": "Los Angeles", "state": "CA", "postalCode": "10001" }, "seller": { "sor": "", "sorCompanyId": "", "autoFillFlag": "" }, "customer": { "sor": "IGEN", "sorCompanyId": "192837467", "autoFillFlag": "Y" } }] });

var config = {
    method: 'post',
    url: 'https://test-api.igenfuels.com/v1.2/taxes/determineTobaccoTax',
    headers: {
        'x-api-key': 'test',
        'Content-Type': 'application/json'
    },
    data: data
};

axios(config)
    .then(function (response) {
        let data = response.data
        // console.log(JSON.stringify(response.data));
        // $.taxResults.0.eventLineResults.taxes.0

        console.log("The response is")
        console.log(data.taxResults)

        let tax_results = data.taxResults[0].eventLineResults

        for(let result of tax_results){
            for(let tax of result.taxes){
                // 
                console.log( "TAX:  ", tax.taxCalculatedValue)
            }

        }
    })
    .catch(function (error) {
        console.log(error);
    });
