var express = require('express');
var router = express.Router();
let gateway = require("../controllers/gateway")

// health check

router.get('/', function(req, res){
  res.status(200).json({
    success: true,
    message: "Welcome to batch program api server",
  })
})


// logging middleware
function loggingMiddleware(req, res, next) {
  // 
  console.log("=============================================")
  console.log("loggingMiddleware Logging in progress")
  console.log("Query")
  console.log(req.query)
  console.log("=============================================")
  console.log("Body")
  console.log(req.body)
  console.log("=============================================")
  console.log("Headers")
  console.log(req.headers)
  console.log("=============================================")
  console.log("loggingMiddleware ended")
  console.log("=============================================")
  next()

}

// router.use(loggingMiddleware)

// production endpoints
router.post("/receive", loggingMiddleware, gateway.receiveRequest)
router.get("/receive", loggingMiddleware, gateway.receiveRequest)


module.exports = router;
