

function receiveRequest(req, res){
    console.log("The request has been received")
    res.status(200).json({
        success: true,
        message: "OK"
    })
}

module.exports = {
    receiveRequest
}