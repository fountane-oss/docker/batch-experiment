APP_NAME=batch-program-gateway-api

run:
	npm run start:dev

install:
	npm install

push:
	git add --all
	git commit
	git push -u origin master
	
pull:
	git pull -v origin master

update:
	make pull
	make restart

pm2-start:
	pm2 start --name $(APP_NAME) bin/www

stop:
	pm2 stop $(APP_NAME)

restart:
	# pm2 restart $(APP_NAME) 
	/root/.nvm/versions/node/v12.13.0/bin/pm2 restart $(APP_NAME)

logs:
	pm2 logs $(APP_NAME) 

deploy:
	ansible-playbook -vvv -i ./ansible/deploy/inventory.ini ./ansible/deploy/playbook.yml

ansible-ping:
	ansible -i ./ansible/deploy/inventory.ini all -m ping
