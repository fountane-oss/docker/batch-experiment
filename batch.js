let axios = require('axios');

/**
 * Environment Variables
 * api_url
 * api_base_domain
 * max_runs
 */

let url = 'http://0.0.0.0:3010/receive'

if(process.env.api_url){
    url = process.env.api_url
} else if (process.env.api_base_domain){
    url = process.env.api_base_domain + '/receive'
}

console.log("The url is")
console.log(url)

let data = JSON.stringify({ "foo": "bar" });

let config = {
    method: 'post',
    url,
    headers: {
        'Content-Type': 'application/json'
    },
    data: data
};



function main(){
    // 
    let max_runs = parseInt(process.env.max_runs) || 10

    console.log("running for ", max_runs, " times")

    for(let i=0; i<max_runs; i++){
        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}


if(require.main == module){
    main()
}