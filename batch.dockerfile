FROM node:15.8.0-alpine3.10

RUN mkdir -p /app
WORKDIR /app

COPY batch.js /app
COPY package.json /app

RUN npm install

CMD ["npm", "run", "batch"]

